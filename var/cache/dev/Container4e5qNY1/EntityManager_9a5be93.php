<?php

namespace Container4e5qNY1;
include_once \dirname(__DIR__, 4).'/vendor/doctrine/persistence/src/Persistence/ObjectManager.php';
include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/EntityManagerInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/EntityManager.php';

class EntityManager_9a5be93 extends \Doctrine\ORM\EntityManager implements \ProxyManager\Proxy\VirtualProxyInterface
{
    /**
     * @var \Doctrine\ORM\EntityManager|null wrapped object, if the proxy is initialized
     */
    private $valueHolder27924 = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializerc8bff = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicProperties0eda9 = [
        
    ];

    public function getConnection()
    {
        $this->initializerc8bff && ($this->initializerc8bff->__invoke($valueHolder27924, $this, 'getConnection', array(), $this->initializerc8bff) || 1) && $this->valueHolder27924 = $valueHolder27924;

        return $this->valueHolder27924->getConnection();
    }

    public function getMetadataFactory()
    {
        $this->initializerc8bff && ($this->initializerc8bff->__invoke($valueHolder27924, $this, 'getMetadataFactory', array(), $this->initializerc8bff) || 1) && $this->valueHolder27924 = $valueHolder27924;

        return $this->valueHolder27924->getMetadataFactory();
    }

    public function getExpressionBuilder()
    {
        $this->initializerc8bff && ($this->initializerc8bff->__invoke($valueHolder27924, $this, 'getExpressionBuilder', array(), $this->initializerc8bff) || 1) && $this->valueHolder27924 = $valueHolder27924;

        return $this->valueHolder27924->getExpressionBuilder();
    }

    public function beginTransaction()
    {
        $this->initializerc8bff && ($this->initializerc8bff->__invoke($valueHolder27924, $this, 'beginTransaction', array(), $this->initializerc8bff) || 1) && $this->valueHolder27924 = $valueHolder27924;

        return $this->valueHolder27924->beginTransaction();
    }

    public function getCache()
    {
        $this->initializerc8bff && ($this->initializerc8bff->__invoke($valueHolder27924, $this, 'getCache', array(), $this->initializerc8bff) || 1) && $this->valueHolder27924 = $valueHolder27924;

        return $this->valueHolder27924->getCache();
    }

    public function transactional($func)
    {
        $this->initializerc8bff && ($this->initializerc8bff->__invoke($valueHolder27924, $this, 'transactional', array('func' => $func), $this->initializerc8bff) || 1) && $this->valueHolder27924 = $valueHolder27924;

        return $this->valueHolder27924->transactional($func);
    }

    public function wrapInTransaction(callable $func)
    {
        $this->initializerc8bff && ($this->initializerc8bff->__invoke($valueHolder27924, $this, 'wrapInTransaction', array('func' => $func), $this->initializerc8bff) || 1) && $this->valueHolder27924 = $valueHolder27924;

        return $this->valueHolder27924->wrapInTransaction($func);
    }

    public function commit()
    {
        $this->initializerc8bff && ($this->initializerc8bff->__invoke($valueHolder27924, $this, 'commit', array(), $this->initializerc8bff) || 1) && $this->valueHolder27924 = $valueHolder27924;

        return $this->valueHolder27924->commit();
    }

    public function rollback()
    {
        $this->initializerc8bff && ($this->initializerc8bff->__invoke($valueHolder27924, $this, 'rollback', array(), $this->initializerc8bff) || 1) && $this->valueHolder27924 = $valueHolder27924;

        return $this->valueHolder27924->rollback();
    }

    public function getClassMetadata($className)
    {
        $this->initializerc8bff && ($this->initializerc8bff->__invoke($valueHolder27924, $this, 'getClassMetadata', array('className' => $className), $this->initializerc8bff) || 1) && $this->valueHolder27924 = $valueHolder27924;

        return $this->valueHolder27924->getClassMetadata($className);
    }

    public function createQuery($dql = '')
    {
        $this->initializerc8bff && ($this->initializerc8bff->__invoke($valueHolder27924, $this, 'createQuery', array('dql' => $dql), $this->initializerc8bff) || 1) && $this->valueHolder27924 = $valueHolder27924;

        return $this->valueHolder27924->createQuery($dql);
    }

    public function createNamedQuery($name)
    {
        $this->initializerc8bff && ($this->initializerc8bff->__invoke($valueHolder27924, $this, 'createNamedQuery', array('name' => $name), $this->initializerc8bff) || 1) && $this->valueHolder27924 = $valueHolder27924;

        return $this->valueHolder27924->createNamedQuery($name);
    }

    public function createNativeQuery($sql, \Doctrine\ORM\Query\ResultSetMapping $rsm)
    {
        $this->initializerc8bff && ($this->initializerc8bff->__invoke($valueHolder27924, $this, 'createNativeQuery', array('sql' => $sql, 'rsm' => $rsm), $this->initializerc8bff) || 1) && $this->valueHolder27924 = $valueHolder27924;

        return $this->valueHolder27924->createNativeQuery($sql, $rsm);
    }

    public function createNamedNativeQuery($name)
    {
        $this->initializerc8bff && ($this->initializerc8bff->__invoke($valueHolder27924, $this, 'createNamedNativeQuery', array('name' => $name), $this->initializerc8bff) || 1) && $this->valueHolder27924 = $valueHolder27924;

        return $this->valueHolder27924->createNamedNativeQuery($name);
    }

    public function createQueryBuilder()
    {
        $this->initializerc8bff && ($this->initializerc8bff->__invoke($valueHolder27924, $this, 'createQueryBuilder', array(), $this->initializerc8bff) || 1) && $this->valueHolder27924 = $valueHolder27924;

        return $this->valueHolder27924->createQueryBuilder();
    }

    public function flush($entity = null)
    {
        $this->initializerc8bff && ($this->initializerc8bff->__invoke($valueHolder27924, $this, 'flush', array('entity' => $entity), $this->initializerc8bff) || 1) && $this->valueHolder27924 = $valueHolder27924;

        return $this->valueHolder27924->flush($entity);
    }

    public function find($className, $id, $lockMode = null, $lockVersion = null)
    {
        $this->initializerc8bff && ($this->initializerc8bff->__invoke($valueHolder27924, $this, 'find', array('className' => $className, 'id' => $id, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializerc8bff) || 1) && $this->valueHolder27924 = $valueHolder27924;

        return $this->valueHolder27924->find($className, $id, $lockMode, $lockVersion);
    }

    public function getReference($entityName, $id)
    {
        $this->initializerc8bff && ($this->initializerc8bff->__invoke($valueHolder27924, $this, 'getReference', array('entityName' => $entityName, 'id' => $id), $this->initializerc8bff) || 1) && $this->valueHolder27924 = $valueHolder27924;

        return $this->valueHolder27924->getReference($entityName, $id);
    }

    public function getPartialReference($entityName, $identifier)
    {
        $this->initializerc8bff && ($this->initializerc8bff->__invoke($valueHolder27924, $this, 'getPartialReference', array('entityName' => $entityName, 'identifier' => $identifier), $this->initializerc8bff) || 1) && $this->valueHolder27924 = $valueHolder27924;

        return $this->valueHolder27924->getPartialReference($entityName, $identifier);
    }

    public function clear($entityName = null)
    {
        $this->initializerc8bff && ($this->initializerc8bff->__invoke($valueHolder27924, $this, 'clear', array('entityName' => $entityName), $this->initializerc8bff) || 1) && $this->valueHolder27924 = $valueHolder27924;

        return $this->valueHolder27924->clear($entityName);
    }

    public function close()
    {
        $this->initializerc8bff && ($this->initializerc8bff->__invoke($valueHolder27924, $this, 'close', array(), $this->initializerc8bff) || 1) && $this->valueHolder27924 = $valueHolder27924;

        return $this->valueHolder27924->close();
    }

    public function persist($entity)
    {
        $this->initializerc8bff && ($this->initializerc8bff->__invoke($valueHolder27924, $this, 'persist', array('entity' => $entity), $this->initializerc8bff) || 1) && $this->valueHolder27924 = $valueHolder27924;

        return $this->valueHolder27924->persist($entity);
    }

    public function remove($entity)
    {
        $this->initializerc8bff && ($this->initializerc8bff->__invoke($valueHolder27924, $this, 'remove', array('entity' => $entity), $this->initializerc8bff) || 1) && $this->valueHolder27924 = $valueHolder27924;

        return $this->valueHolder27924->remove($entity);
    }

    public function refresh($entity)
    {
        $this->initializerc8bff && ($this->initializerc8bff->__invoke($valueHolder27924, $this, 'refresh', array('entity' => $entity), $this->initializerc8bff) || 1) && $this->valueHolder27924 = $valueHolder27924;

        return $this->valueHolder27924->refresh($entity);
    }

    public function detach($entity)
    {
        $this->initializerc8bff && ($this->initializerc8bff->__invoke($valueHolder27924, $this, 'detach', array('entity' => $entity), $this->initializerc8bff) || 1) && $this->valueHolder27924 = $valueHolder27924;

        return $this->valueHolder27924->detach($entity);
    }

    public function merge($entity)
    {
        $this->initializerc8bff && ($this->initializerc8bff->__invoke($valueHolder27924, $this, 'merge', array('entity' => $entity), $this->initializerc8bff) || 1) && $this->valueHolder27924 = $valueHolder27924;

        return $this->valueHolder27924->merge($entity);
    }

    public function copy($entity, $deep = false)
    {
        $this->initializerc8bff && ($this->initializerc8bff->__invoke($valueHolder27924, $this, 'copy', array('entity' => $entity, 'deep' => $deep), $this->initializerc8bff) || 1) && $this->valueHolder27924 = $valueHolder27924;

        return $this->valueHolder27924->copy($entity, $deep);
    }

    public function lock($entity, $lockMode, $lockVersion = null)
    {
        $this->initializerc8bff && ($this->initializerc8bff->__invoke($valueHolder27924, $this, 'lock', array('entity' => $entity, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializerc8bff) || 1) && $this->valueHolder27924 = $valueHolder27924;

        return $this->valueHolder27924->lock($entity, $lockMode, $lockVersion);
    }

    public function getRepository($entityName)
    {
        $this->initializerc8bff && ($this->initializerc8bff->__invoke($valueHolder27924, $this, 'getRepository', array('entityName' => $entityName), $this->initializerc8bff) || 1) && $this->valueHolder27924 = $valueHolder27924;

        return $this->valueHolder27924->getRepository($entityName);
    }

    public function contains($entity)
    {
        $this->initializerc8bff && ($this->initializerc8bff->__invoke($valueHolder27924, $this, 'contains', array('entity' => $entity), $this->initializerc8bff) || 1) && $this->valueHolder27924 = $valueHolder27924;

        return $this->valueHolder27924->contains($entity);
    }

    public function getEventManager()
    {
        $this->initializerc8bff && ($this->initializerc8bff->__invoke($valueHolder27924, $this, 'getEventManager', array(), $this->initializerc8bff) || 1) && $this->valueHolder27924 = $valueHolder27924;

        return $this->valueHolder27924->getEventManager();
    }

    public function getConfiguration()
    {
        $this->initializerc8bff && ($this->initializerc8bff->__invoke($valueHolder27924, $this, 'getConfiguration', array(), $this->initializerc8bff) || 1) && $this->valueHolder27924 = $valueHolder27924;

        return $this->valueHolder27924->getConfiguration();
    }

    public function isOpen()
    {
        $this->initializerc8bff && ($this->initializerc8bff->__invoke($valueHolder27924, $this, 'isOpen', array(), $this->initializerc8bff) || 1) && $this->valueHolder27924 = $valueHolder27924;

        return $this->valueHolder27924->isOpen();
    }

    public function getUnitOfWork()
    {
        $this->initializerc8bff && ($this->initializerc8bff->__invoke($valueHolder27924, $this, 'getUnitOfWork', array(), $this->initializerc8bff) || 1) && $this->valueHolder27924 = $valueHolder27924;

        return $this->valueHolder27924->getUnitOfWork();
    }

    public function getHydrator($hydrationMode)
    {
        $this->initializerc8bff && ($this->initializerc8bff->__invoke($valueHolder27924, $this, 'getHydrator', array('hydrationMode' => $hydrationMode), $this->initializerc8bff) || 1) && $this->valueHolder27924 = $valueHolder27924;

        return $this->valueHolder27924->getHydrator($hydrationMode);
    }

    public function newHydrator($hydrationMode)
    {
        $this->initializerc8bff && ($this->initializerc8bff->__invoke($valueHolder27924, $this, 'newHydrator', array('hydrationMode' => $hydrationMode), $this->initializerc8bff) || 1) && $this->valueHolder27924 = $valueHolder27924;

        return $this->valueHolder27924->newHydrator($hydrationMode);
    }

    public function getProxyFactory()
    {
        $this->initializerc8bff && ($this->initializerc8bff->__invoke($valueHolder27924, $this, 'getProxyFactory', array(), $this->initializerc8bff) || 1) && $this->valueHolder27924 = $valueHolder27924;

        return $this->valueHolder27924->getProxyFactory();
    }

    public function initializeObject($obj)
    {
        $this->initializerc8bff && ($this->initializerc8bff->__invoke($valueHolder27924, $this, 'initializeObject', array('obj' => $obj), $this->initializerc8bff) || 1) && $this->valueHolder27924 = $valueHolder27924;

        return $this->valueHolder27924->initializeObject($obj);
    }

    public function getFilters()
    {
        $this->initializerc8bff && ($this->initializerc8bff->__invoke($valueHolder27924, $this, 'getFilters', array(), $this->initializerc8bff) || 1) && $this->valueHolder27924 = $valueHolder27924;

        return $this->valueHolder27924->getFilters();
    }

    public function isFiltersStateClean()
    {
        $this->initializerc8bff && ($this->initializerc8bff->__invoke($valueHolder27924, $this, 'isFiltersStateClean', array(), $this->initializerc8bff) || 1) && $this->valueHolder27924 = $valueHolder27924;

        return $this->valueHolder27924->isFiltersStateClean();
    }

    public function hasFilters()
    {
        $this->initializerc8bff && ($this->initializerc8bff->__invoke($valueHolder27924, $this, 'hasFilters', array(), $this->initializerc8bff) || 1) && $this->valueHolder27924 = $valueHolder27924;

        return $this->valueHolder27924->hasFilters();
    }

    /**
     * Constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;

        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();

        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $instance, 'Doctrine\\ORM\\EntityManager')->__invoke($instance);

        $instance->initializerc8bff = $initializer;

        return $instance;
    }

    protected function __construct(\Doctrine\DBAL\Connection $conn, \Doctrine\ORM\Configuration $config, \Doctrine\Common\EventManager $eventManager)
    {
        static $reflection;

        if (! $this->valueHolder27924) {
            $reflection = $reflection ?? new \ReflectionClass('Doctrine\\ORM\\EntityManager');
            $this->valueHolder27924 = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);

        }

        $this->valueHolder27924->__construct($conn, $config, $eventManager);
    }

    public function & __get($name)
    {
        $this->initializerc8bff && ($this->initializerc8bff->__invoke($valueHolder27924, $this, '__get', ['name' => $name], $this->initializerc8bff) || 1) && $this->valueHolder27924 = $valueHolder27924;

        if (isset(self::$publicProperties0eda9[$name])) {
            return $this->valueHolder27924->$name;
        }

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder27924;

            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }

        $targetObject = $this->valueHolder27924;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __set($name, $value)
    {
        $this->initializerc8bff && ($this->initializerc8bff->__invoke($valueHolder27924, $this, '__set', array('name' => $name, 'value' => $value), $this->initializerc8bff) || 1) && $this->valueHolder27924 = $valueHolder27924;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder27924;

            $targetObject->$name = $value;

            return $targetObject->$name;
        }

        $targetObject = $this->valueHolder27924;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;

            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __isset($name)
    {
        $this->initializerc8bff && ($this->initializerc8bff->__invoke($valueHolder27924, $this, '__isset', array('name' => $name), $this->initializerc8bff) || 1) && $this->valueHolder27924 = $valueHolder27924;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder27924;

            return isset($targetObject->$name);
        }

        $targetObject = $this->valueHolder27924;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __unset($name)
    {
        $this->initializerc8bff && ($this->initializerc8bff->__invoke($valueHolder27924, $this, '__unset', array('name' => $name), $this->initializerc8bff) || 1) && $this->valueHolder27924 = $valueHolder27924;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder27924;

            unset($targetObject->$name);

            return;
        }

        $targetObject = $this->valueHolder27924;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);

            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }

    public function __clone()
    {
        $this->initializerc8bff && ($this->initializerc8bff->__invoke($valueHolder27924, $this, '__clone', array(), $this->initializerc8bff) || 1) && $this->valueHolder27924 = $valueHolder27924;

        $this->valueHolder27924 = clone $this->valueHolder27924;
    }

    public function __sleep()
    {
        $this->initializerc8bff && ($this->initializerc8bff->__invoke($valueHolder27924, $this, '__sleep', array(), $this->initializerc8bff) || 1) && $this->valueHolder27924 = $valueHolder27924;

        return array('valueHolder27924');
    }

    public function __wakeup()
    {
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);
    }

    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializerc8bff = $initializer;
    }

    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializerc8bff;
    }

    public function initializeProxy() : bool
    {
        return $this->initializerc8bff && ($this->initializerc8bff->__invoke($valueHolder27924, $this, 'initializeProxy', array(), $this->initializerc8bff) || 1) && $this->valueHolder27924 = $valueHolder27924;
    }

    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder27924;
    }

    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder27924;
    }
}

if (!\class_exists('EntityManager_9a5be93', false)) {
    \class_alias(__NAMESPACE__.'\\EntityManager_9a5be93', 'EntityManager_9a5be93', false);
}
