<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* base.html.twig */
class __TwigTemplate_07778b03eb7a9722fc53f5582896c6a8 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'stylesheets' => [$this, 'block_stylesheets'],
            'javascripts' => [$this, 'block_javascripts'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "base.html.twig"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
\t<head>
\t\t<meta charset=\"UTF-8\">
\t\t<title>
\t\t\t";
        // line 6
        $this->displayBlock('title', $context, $blocks);
        // line 8
        echo "\t\t</title>
\t\t<link rel=\"icon\" href=\"data:image/svg+xml,<svg xmlns=%22http://www.w3.org/2000/svg%22 viewBox=%220 0 128 128%22><text y=%221.2em%22 font-size=%2296%22>⚫️</text></svg>\">
\t\t<link href=\"https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css\" rel=\"stylesheet\" integrity=\"sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3\" crossorigin=\"anonymous\">
\t\t<script src=\"https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js\" integrity=\"sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p\" crossorigin=\"anonymous\"></script>
\t\t";
        // line 13
        echo "
\t\t";
        // line 14
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 17
        echo "
\t\t";
        // line 18
        $this->displayBlock('javascripts', $context, $blocks);
        // line 21
        echo "\t</head>
\t<body>
\t\t<style>
\t\t\thtml {
\t\t\t\tscroll-behavior: smooth;
\t\t\t}
\t\t\tbody {
\t\t\t\tmargin: 0;
\t\t\t\tpadding: 70px 0 0;
\t\t\t\tbox-sizing: border-box;
\t\t\t\toverflow-x: hidden;
\t\t\t\tfont-family: 'Lato', sans-serif;
\t\t\t\theight: 100vh;
\t\t\t}

\t\t\th1,
\t\t\th2,
\t\t\th3,
\t\t\th4,
\t\t\th5,
\t\t\th6 {
\t\t\t\tcolor: var(--color1);
\t\t\t\ttext-transform: uppercase;
\t\t\t\tfont-family: 'Montserrat', sans-serif;
\t\t\t}
\t\t\t:root {
\t\t\t\t--color1: rgb(21, 104, 90);
\t\t\t\t--color2: rgb(108, 117, 125);
\t\t\t\t--color3: white;
\t\t\t}
\t\t\t.btn-primary:hover {
\t\t\t\tcolor: var(--color3);
\t\t\t\tbackground-color: var(--color1);
\t\t\t\tborder-color: var(--color1);
\t\t\t}
\t\t\t.btn-primary {
\t\t\t\tcolor: var(--color1);
\t\t\t\tbackground-color: var(--color3);
\t\t\t\tborder-color: var(--color1);
\t\t\t}
\t\t\t/*NAVBAR*/
\t\t\t.navbar {
\t\t\t\ttext-transform: uppercase;
\t\t\t\tfont-size: 14px;
\t\t\t\tfont-weight: 700;
\t\t\t\tletter-spacing: 0.2rem;
\t\t\t\tpadding: 20px 0;
\t\t\t}

\t\t\t.navbar-light .navbar-nav .nav-link {
\t\t\t\tpadding-left: 20px;
\t\t\t}

\t\t\t.navbar-light .navbar-nav .nav-link.active,
\t\t\t.navbar-light .navbar-nav .nav-link:hover {
\t\t\t\tcolor: var(--color1)
\t\t\t}
\t\t</style>
\t\t<nav class=\"navbar fixed-top navbar-expand-lg navbar-light bg-white\">
\t\t\t<div class=\"container\">
\t\t\t\t<button class=\"navbar-toggler\" data-bs-toggle=\"collapse\" data-bs-target=\"#navbarToggler\" type=\"button\" aria-controls=\"navbarToggler\" aria-label=\"Toggle navigation\" aria-expanded=\"false\">
\t\t\t\t\t<span class=\"navbar-toggler-icon\"></span>
\t\t\t\t</button>

\t\t\t\t<div class=\"collapse navbar-collapse\" id=\"navbarToggler\">
\t\t\t\t\t<ul class=\"navbar-nav mx-auto \">
\t\t\t\t\t\t<li class=\"nav-item\">
\t\t\t\t\t\t\t<a class=\"nav-link\" href=\"";
        // line 88
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("appHome");
        echo "\">Accueil</a>
\t\t\t\t\t\t</li>
\t\t\t\t\t\t<li class=\"nav-item\">
\t\t\t\t\t\t\t<a class=\"nav-link\" href=\"";
        // line 91
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("appPortfolio");
        echo "\">Portfolio</a>
\t\t\t\t\t\t</li>
\t\t\t\t\t</ul>
\t\t\t\t</div>
\t\t\t</div>
\t\t</nav>
\t\t";
        // line 97
        $this->displayBlock('body', $context, $blocks);
        // line 98
        echo "\t</body>
</html></body></html>
";
        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 6
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!
\t\t\t";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

    }

    // line 14
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 15
        echo "\t\t\t";
        echo twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('encore_entry_link_tags')->getCallable(), ["app"]), "html", null, true);
        echo "
\t\t";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

    }

    // line 18
    public function block_javascripts($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        // line 19
        echo "\t\t\t";
        echo twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('encore_entry_script_tags')->getCallable(), ["app"]), "html", null, true);
        echo "
\t\t";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

    }

    // line 97
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  236 => 97,  223 => 19,  213 => 18,  200 => 15,  190 => 14,  170 => 6,  158 => 98,  156 => 97,  147 => 91,  141 => 88,  72 => 21,  70 => 18,  67 => 17,  65 => 14,  62 => 13,  56 => 8,  54 => 6,  47 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!DOCTYPE html>
<html>
\t<head>
\t\t<meta charset=\"UTF-8\">
\t\t<title>
\t\t\t{% block title %}Welcome!
\t\t\t{% endblock %}
\t\t</title>
\t\t<link rel=\"icon\" href=\"data:image/svg+xml,<svg xmlns=%22http://www.w3.org/2000/svg%22 viewBox=%220 0 128 128%22><text y=%221.2em%22 font-size=%2296%22>⚫️</text></svg>\">
\t\t<link href=\"https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css\" rel=\"stylesheet\" integrity=\"sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3\" crossorigin=\"anonymous\">
\t\t<script src=\"https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js\" integrity=\"sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p\" crossorigin=\"anonymous\"></script>
\t\t{# Run `composer require symfony/webpack-encore-bundle` to start using Symfony UX #}

\t\t{% block stylesheets %}
\t\t\t{{ encore_entry_link_tags('app') }}
\t\t{% endblock %}

\t\t{% block javascripts %}
\t\t\t{{ encore_entry_script_tags('app') }}
\t\t{% endblock %}
\t</head>
\t<body>
\t\t<style>
\t\t\thtml {
\t\t\t\tscroll-behavior: smooth;
\t\t\t}
\t\t\tbody {
\t\t\t\tmargin: 0;
\t\t\t\tpadding: 70px 0 0;
\t\t\t\tbox-sizing: border-box;
\t\t\t\toverflow-x: hidden;
\t\t\t\tfont-family: 'Lato', sans-serif;
\t\t\t\theight: 100vh;
\t\t\t}

\t\t\th1,
\t\t\th2,
\t\t\th3,
\t\t\th4,
\t\t\th5,
\t\t\th6 {
\t\t\t\tcolor: var(--color1);
\t\t\t\ttext-transform: uppercase;
\t\t\t\tfont-family: 'Montserrat', sans-serif;
\t\t\t}
\t\t\t:root {
\t\t\t\t--color1: rgb(21, 104, 90);
\t\t\t\t--color2: rgb(108, 117, 125);
\t\t\t\t--color3: white;
\t\t\t}
\t\t\t.btn-primary:hover {
\t\t\t\tcolor: var(--color3);
\t\t\t\tbackground-color: var(--color1);
\t\t\t\tborder-color: var(--color1);
\t\t\t}
\t\t\t.btn-primary {
\t\t\t\tcolor: var(--color1);
\t\t\t\tbackground-color: var(--color3);
\t\t\t\tborder-color: var(--color1);
\t\t\t}
\t\t\t/*NAVBAR*/
\t\t\t.navbar {
\t\t\t\ttext-transform: uppercase;
\t\t\t\tfont-size: 14px;
\t\t\t\tfont-weight: 700;
\t\t\t\tletter-spacing: 0.2rem;
\t\t\t\tpadding: 20px 0;
\t\t\t}

\t\t\t.navbar-light .navbar-nav .nav-link {
\t\t\t\tpadding-left: 20px;
\t\t\t}

\t\t\t.navbar-light .navbar-nav .nav-link.active,
\t\t\t.navbar-light .navbar-nav .nav-link:hover {
\t\t\t\tcolor: var(--color1)
\t\t\t}
\t\t</style>
\t\t<nav class=\"navbar fixed-top navbar-expand-lg navbar-light bg-white\">
\t\t\t<div class=\"container\">
\t\t\t\t<button class=\"navbar-toggler\" data-bs-toggle=\"collapse\" data-bs-target=\"#navbarToggler\" type=\"button\" aria-controls=\"navbarToggler\" aria-label=\"Toggle navigation\" aria-expanded=\"false\">
\t\t\t\t\t<span class=\"navbar-toggler-icon\"></span>
\t\t\t\t</button>

\t\t\t\t<div class=\"collapse navbar-collapse\" id=\"navbarToggler\">
\t\t\t\t\t<ul class=\"navbar-nav mx-auto \">
\t\t\t\t\t\t<li class=\"nav-item\">
\t\t\t\t\t\t\t<a class=\"nav-link\" href=\"{{ path('appHome') }}\">Accueil</a>
\t\t\t\t\t\t</li>
\t\t\t\t\t\t<li class=\"nav-item\">
\t\t\t\t\t\t\t<a class=\"nav-link\" href=\"{{ path('appPortfolio') }}\">Portfolio</a>
\t\t\t\t\t\t</li>
\t\t\t\t\t</ul>
\t\t\t\t</div>
\t\t\t</div>
\t\t</nav>
\t\t{% block body %}{% endblock %}
\t</body>
</html></body></html>
", "base.html.twig", "/home/naima/portfolio/templates/base.html.twig");
    }
}
