<?php

namespace App\Controller\Admin;
use App\Entity\Projets;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Persistence\ManagerRegistry;



class AdminController extends AbstractController
{
    #[Route('/admin', name: 'admin')]
    public function index( ManagerRegistry $doctrine, Request $request): Response
    {
        $request= Request:: createFromGlobals();
        $entityManager = $doctrine -> getManager();
        if ($request -> request -> get('title')){
            $addProjet = new Projets();
            $addProjet -> setTitle($request -> request -> get('title'));
            $addProjet -> setDescription($request -> request -> get('description'));
            $addProjet -> setImgUrl1($request -> request -> get('url1'));
            $addProjet -> setImgUrl2($request -> request -> get('url2'));
            $addProjet -> setLinkRepository($request -> request -> get('linkRepo'));
            $addProjet -> setLinkSite($request -> request -> get('linkSite'));
            $entityManager -> persist($addProjet);
            $entityManager ->flush();

        }
        // Option 1. You can make your dashboard redirect to some common page of your backend
        //
        // $adminUrlGenerator = $this->container->get(AdminUrlGenerator::class);
        // return $this->redirect($adminUrlGenerator->setController(OneOfYourCrudController::class)->generateUrl());

        // Option 2. You can make your dashboard redirect to different pages depending on the user
        //
        // if ('jane' === $this->getUser()->getUsername()) {
        //     return $this->redirect('...');
        // }

        // Option 3. You can render some custom template to display a proper dashboard with widgets, etc.
        // (tip: it's easier if your template extends from @EasyAdmin/page/content.html.twig)
        //
        return $this->render('admin/admin.html.twig');
    }

    #[Route('/admin/newproject', name: 'admin.new', methods: ['GET', 'POST'])]
    public function new(): Response
    {
        return $this->render('projet/admin-newproject.html.twig');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linkToDashboard('Dashboard', 'fa fa-home');
        // yield MenuItem::linkToCrud('The Label', 'fas fa-list', EntityClass::class);
    }
}
